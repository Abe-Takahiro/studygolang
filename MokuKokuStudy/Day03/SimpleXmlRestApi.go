package main

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
)

type Profile struct {
	Name    string
	Hobbies []string `xml:"Hobbies>Hobby"`
}

func main() {
	http.HandleFunc("/", rootResponse)
	http.HandleFunc("/greet", greetResponse)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func rootResponse(w http.ResponseWriter, r *http.Request) {
	profile := Profile{"Hoge", []string{"sumo", "to be on the pull"}}

	x, err := xml.MarshalIndent(profile, "", " ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Context-Type", "application/xml")
	w.Write(x)
}

func greetResponse(w http.ResponseWriter, r *http.Request) {
	profile := Profile{}
	switch r.Method {
	case http.MethodGet:
		profile = Profile{"Hoge", []string{"sumo", "to be on the pull"}}
	case http.MethodPut:
		rb, err := ioutil.ReadAll(r.Body)
		if err == nil {
			log.Println(rb)
		}
		profile = Profile{"Foo", []string{"guiter", "banana"}}
	}

	x, err := xml.MarshalIndent(profile, "", " ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Context-Type", "application/xml")
	w.Write(x)
}
