package main

import (
	"fmt"
	"strconv"
)

func main() {
	var msg string
	for i := 1; i <= 30; i++ {
		if i%15 == 0 {
			fmt.Printf("%dFizzBuzz\n", i)
		} else if i%3 == 0 {
			//fmt.Printf("%dFizz\n", i)
			fmt.Print(strconv.Itoa(i) + "Fizz\n")
		} else if i%5 == 0 {
			//fmt.Printf("%dBuzz\n", i)
			//fmt.Print(strconv.Itoa(i) + "Buzz\n")
			msg = strconv.Itoa(i) + "Buzz\n"
			fmt.Print(msg)
		} else {
			fmt.Println(i)
		}
	}
}
