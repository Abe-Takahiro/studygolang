package main

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
)

type Profile struct {
	Name    string
	Hobbies []string `xml:"Hobbies>Hobby"`
}

type ResponseXml struct {
	Status  string
	Profile Profile
}

func main() {
	http.HandleFunc("/", rootResponse)
	http.HandleFunc("/greet", greetResponse)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func rootResponse(w http.ResponseWriter, r *http.Request) {
	profile := Profile{"Hoge", []string{"sumo", "to be on the pull"}}

	x, err := xml.MarshalIndent(profile, "", " ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Context-Type", "application/xml")
	w.Write(x)
}

func greetResponse(w http.ResponseWriter, r *http.Request) {
	profile := Profile{}
	resXml := ResponseXml{}
	switch r.Method {
	case http.MethodGet:
		profile = Profile{"Hoge", []string{"sumo", "to be on the pull"}}
		resXml.Status = "OK"
		resXml.Profile = profile
	case http.MethodPut:
		rb, err := ioutil.ReadAll(r.Body)
		if err == nil {
			xml.Unmarshal(rb, &profile)
			log.Print(profile)
			resXml.Status = "OK"
			resXml.Profile = profile
		} else {
			resXml.Status = "NG"
		}
	}

	x, err := xml.MarshalIndent(resXml, "", " ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Context-Type", "application/xml")
	w.Write(x)
}
