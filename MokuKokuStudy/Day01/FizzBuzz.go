package main

import "fmt"

func main() {
	for i := 1; i <= 30; i++ {
		fmt.Print("i=")
		fmt.Print(i)
		if i%15 == 0 {
			fmt.Print("::FizzBuzz")
		} else if i%3 == 0 {
			fmt.Print("::Fizz")
		} else if i%5 == 0 {
			fmt.Print("::Buzz")
		}
		fmt.Println("")
	}
}
